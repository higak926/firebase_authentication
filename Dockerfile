#Node.js & Yarn
FROM node:13.5-alpine as node

RUN apk add --no-cache bash curl && \
    curl -o- -L https://yarnpkg.com/install.sh | bash -s -- --version 1.21.1

#Ruby & Bundler & mysql-client
FROM ruby:2.6.5-alpine

COPY --from=node /usr/local/bin/node /usr/local/bin/node
COPY --from=node /opt/yarn-* /opt/yarn
RUN ln -fs /opt/yarn/bin/yarn /usr/local/bin/yarn

ENV APP_ROOT /myapp

ENV LANG=ja_JP.UTF-8 \
    BUNDLE_JOBS=4

RUN gem update --system && \
    gem install --no-document bundler:2.1.4

RUN apk add --no-cache \
	libxml2-dev \
	libxslt-dev \
	tzdata \
	bash \
        mysql-client \
        mysql-dev \
	less && \
    apk add --virtual build-packs --no-cache \
    alpine-sdk \
    sqlite-dev && \
    cp /usr/share/zoneinfo/Asia/Tokyo /etc/localtime

WORKDIR $APP_ROOT

COPY Gemfile $APP_ROOT/Gemfile
COPY Gemfile.lock $APP_ROOT/Gemfile.lock
RUN bundle install && \
    apk del build-packs && \
    rm -rf /usr/local/bundle/cache/* /usr/local/share/.cache/* /var/cache/* /tmp/*
COPY . $APP_ROOT

# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]
